#ifndef _SHA_256_H
#define _SHA_256_H

int sha_256(unsigned char *out, unsigned char *data, int data_len);

#endif
