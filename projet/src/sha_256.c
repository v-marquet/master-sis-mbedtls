#include "sha_256.h"
#include "mbedtls/sha256.h"

#include <string.h>
#include <stdlib.h>

int sha_256(unsigned char *out, unsigned char *data, int data_len)
{
	int ret;
	unsigned char hash[32];
	mbedtls_sha256_context ctx;

	/* *** Init *** */
	ret = 1; // error

	/* *** Check args *** */
	if((out == NULL) || (data == NULL))
		goto cleanup;

	mbedtls_sha256_init(&ctx);
	
	mbedtls_sha256_starts(&ctx, 0);  // 0 = use SHA256

	mbedtls_sha256_update(&ctx, (unsigned char *)data, data_len);
	
	mbedtls_sha256_finish(&ctx, hash);

	memcpy(out, hash, 32);

	ret = 0; // success

cleanup:

	mbedtls_sha256_free(&ctx);
	return ret;
}
