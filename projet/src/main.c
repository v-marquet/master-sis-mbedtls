#include "main.h"
#include "encrypt.h"
#include "decrypt.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>



int main(int argc, char const *argv[])
{
	int ret;

	if (argc < 2) {
		fprintf(stderr, "Error: not enough arguments. Aborting.\n");
		printUsage(argv[0]);
		exit(1);
	}

	if (strcmp(argv[1], "-e") == 0) {
		ret = encrypt(argc, argv);
	} else if (strcmp(argv[1], "-d") == 0) {
		ret = decrypt(argc, argv);
	} else {
		fprintf(stderr, "Error: first argument must be either -e or -d. Aborting.\n");
		printUsage(argv[0]);
		exit(1);
	}

	return ret;
}

void printUsage(const char* name) {
	printf("Usage:\n");
	printf("- Encryption: %s -e <input_file> <output_file> <my_sign_priv.pem> " \
		   "<my_ciph_pub.pem>  [user1_ciph_pub.pem ... [userN_ciph_pub.pem]]\n", name);
	printf("- Decryption: %s -e <input_file> <output_file> <my_sign_priv.pem> " \
		   "<my_ciph_pub.pem> [user1_ciph_pub.pem ... [userN_ciph_pub.pem]]\n", name);
}
