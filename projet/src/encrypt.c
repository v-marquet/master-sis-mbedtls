#include "mbedtls/havege.h"
#include "mbedtls/sha256.h"
#include "mbedtls/md.h"
#include "mbedtls/aes.h"
#include "mbedtls/pk.h"
#include "mbedtls/ctr_drbg.h"

#include "encrypt.h"
#include "main.h"
#include "sha_256.h"
#include "key_gen.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>


#define SHA256_HASH_SZ 32
#define KEY_SZ     32
#define BLOCK_SZ       16
#define IV_SZ          BLOCK_SZ
#define HMAC_SZ        32


int encrypt(int argc, char const *argv[]) {
	char* input;
	char* output_ciphered;
	int i;
	int ret;

	if (argc < 6) {
		fprintf(stderr, "Error: number of arguments should be >= 6. Aborting.\n");
		printUsage(argv[0]);
		exit(1);
	}

	int nb_destin = argc - 5;  // nombre de destinataires


	/*** init random number generator necessary for RSA ***/
	mbedtls_ctr_drbg_context ctr_drbg;
	mbedtls_ctr_drbg_init(&ctr_drbg);


	/*** generate random Kc (encryption key) and IV ***/
	unsigned char* key = malloc(sizeof(unsigned char*)*KEY_SZ);
	ret = key_gen(key, KEY_SZ);
	if (ret != 0) {
		fprintf(stderr, "Error: key_gen failed. Aborting.\n");
		exit(1);
	}

	unsigned char* iv = malloc(sizeof(unsigned char*)*IV_SZ);
	ret = key_gen(iv, IV_SZ);
	if (ret != 0) {
		fprintf(stderr, "Error: key_gen failed. Aborting.\n");
		exit(1);
	}


	/*** we read and hash each public key ***/
	for (i=0; i<nb_destin; i++) {
		// TODO: 0x00

		// open file for reading
		FILE* kpub_file = fopen(argv[5+i], "r");
		if (kpub_file == NULL) {
			fprintf(stderr, "Error: can't open file %s. Aborting.\n", argv[5+i]);
			exit(1);
		}

		// get file size, so we can create a buffer with exact size
		fseek(kpub_file, 0L, SEEK_END);
		int kpub_size = ftell(kpub_file);
		fseek(kpub_file, 0L, SEEK_SET);  // put pointer at start again

		// fill buffer with file content
		unsigned char* kpub_buffer = malloc(sizeof(unsigned char) * kpub_size);
		fread(&kpub_buffer[i], 1, kpub_size, kpub_file);

		fclose(kpub_file);

		// compute SHA256
		unsigned char* hash = malloc(sizeof(unsigned char*)*32);
		ret = sha_256(hash, kpub_buffer, kpub_size);
		if (ret != 0) {
			fprintf(stderr, "Error: sha_256 failed. Aborting.\n");
			exit(1);
		}

		// RSA_kpub-i(Kc)
		mbedtls_pk_context pk;
		mbedtls_pk_init(&pk);
		if((ret = mbedtls_pk_parse_public_keyfile(&pk, "our-key.pub")) != 0) {
			fprintf(stderr, "Error: mbedtls_pk_parse_public_keyfile failed for %s. Aborting.\n", argv[5+i]);
			exit(1);
		}

		int key_ciphered_max_len = sizeof(unsigned char*) * MBEDTLS_MPI_MAX_SIZE;
		unsigned char* key_ciphered = malloc(key_ciphered_max_len);
		size_t key_ciphered_len;
		if((ret = mbedtls_pk_encrypt(&pk, key, KEY_SZ, key_ciphered, &key_ciphered_len, key_ciphered_max_len,
										mbedtls_ctr_drbg_random, &ctr_drbg ) ) != 0 ) {
			fprintf(stderr, "Error: mbedtls_pk_encrypt failed. Aborting.\n");
			exit(1);
		}


		free(kpub_buffer);
		free(key_ciphered);
		free(hash);
	}




	memset(key, 0x00, KEY_SZ);
	free(key);
	key = NULL;

	return 0;
}
