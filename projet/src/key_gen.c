#include "mbedtls/havege.h"

#include "key_gen.h"

#include <string.h>


int key_gen(unsigned char *key, int key_length)
{
	int ret;
	mbedtls_havege_state ctx;

	ret = 1; //error

	/* *** check argument *** */
	if((key == NULL) || (key_length <= 0))
		goto cleanup;

	mbedtls_havege_init(&ctx);
	ret = mbedtls_havege_random(&ctx, key, key_length);
	mbedtls_havege_free(&ctx);
    
cleanup:
	return ret;
}
