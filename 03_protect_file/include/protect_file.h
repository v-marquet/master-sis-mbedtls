#ifndef _PROTECT_FILE_H
#define _PROTECT_FILE_H

#include "mbedtls/aes.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @param [out] output      ciphered buffer
 * @param [out] output_len  ciphered buffer length in bytes
 * @param [in]  input       plain text buffer
 * @param [in]  input_len   plain text buffer length in bytes
 * @param [in]  master_key  master key (km)
 * @param [in]  key_len     master key length in bytes
 * @param [in]  salt        salt
 * @param [in]  salt_len    salt length in bytes
 * @return      0 if OK, else 1
 */
int protect_buffer(unsigned char** output, int* output_len,
                   unsigned char* input, int input_len,
                   unsigned char* master_key, int key_len,
                   unsigned char* salt, int salt_len);
 


#endif
