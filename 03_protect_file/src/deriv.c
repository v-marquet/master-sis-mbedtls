#include "deriv.h"
#include "mbedtls/sha256.h"

#include <string.h>
#include <stdlib.h>

int deriv_passwd(unsigned char *key,
		char *password,
		unsigned char *salt, int salt_len,
		unsigned int iterations)
{
	int ret;
	unsigned int i;
	unsigned char hash[32];
	mbedtls_sha256_context ctx;

	/* *** Init *** */
	ret = 1; // error
	i = 0;

	/* *** Check args *** */
	if((key == NULL) || (password == NULL) || (salt == NULL) 
			|| (salt_len <= 0) || (iterations == 0))
		goto cleanup;

    mbedtls_sha256_init(&ctx);
    
	/* *** Get H0 *** */
	mbedtls_sha256_starts(&ctx, 0);

	mbedtls_sha256_update(&ctx, (unsigned char *)password, strlen(password));
	mbedtls_sha256_update(&ctx, salt, salt_len);
	mbedtls_sha256_update(&ctx, (unsigned char *)&i, sizeof(int));
	
	mbedtls_sha256_finish(&ctx, hash); //hash == H0

	/* *** Hi *** */
	for(i = 1; i < iterations; i++)
	{
		mbedtls_sha256_starts(&ctx, 0);

		mbedtls_sha256_update(&ctx, hash, 32);
		mbedtls_sha256_update(&ctx, (unsigned char *)password, strlen(password));
		mbedtls_sha256_update(&ctx, salt, salt_len);
		mbedtls_sha256_update(&ctx, (unsigned char *)&i, sizeof(int));

		mbedtls_sha256_finish(&ctx, hash);
	}
	memcpy(key, hash, 32);

	ret = 0; // success

cleanup:

	mbedtls_sha256_free(&ctx);
	return ret;
}
