#include "protect_file.h"
#include "gen_key.h"


int protect_buffer(unsigned char** output, int* output_len,
                   unsigned char* input, int input_len,
                   unsigned char* master_key, int key_len,
                   unsigned char* salt, int salt_len) {

    int ret;

    mbedtls_aes_context context;
    mbedtls_aes_init(&context);

    mbedtls_aes_setkey_enc(&context, master_key, key_len);

    // IV creation
    int iv_length = 16;
    unsigned char* iv = (unsigned char *)malloc(iv_length * sizeof(unsigned char));
    ret = gen_key(iv, iv_length);
    if (ret != 0) {
        printf("error: gen_key\n");
        return 1;
    }


    // add padding
    int padding_len = 16 - (input_len % 16);
    unsigned char* input_padded = malloc(sizeof(unsigned char) * (input_len + padding_len));
    memcpy(input_padded, input, input_len);
    input_padded[input_len] = '\x80';
    int i;
    for (i=(input_len+1); i<padding_len; i++) {
        input_padded[i] = '\x00';
    }

    // encryption (input data length must be a multiple of 16 bytes)
    mbedtls_aes_crypt_cbc(&context, MBEDTLS_AES_ENCRYPT, input_len, iv, input, *output);
    *output_len = input_len + padding_len;


    mbedtls_aes_free(&context);
    return 0;
}
