#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "protect_file.h"
#include "gen_key.h"
#include "deriv.h"

#define SALT_SZ             8
#define KEY_SZ              32
#define DEFAULT_ITERATION   6000

int main(int argc, char const *argv[])
{
    int ret;

    ////// first we have to create the master key, like in the previous programs
    // 1: we get the salt like in program 01
    int salt_length = 5;
    unsigned char* salt = (unsigned char *)malloc(salt_length * sizeof(unsigned char));
    ret = gen_key(salt, salt_length);
    if (ret != 0) {
        printf("error: gen_key\n");
        return 1;
    }

    // 2: we use program 02 to derivate the password and the salt into a master key
    unsigned char master_key[KEY_SZ];
    char* password = "toto";
    ret = deriv_passwd(master_key, password, salt, salt_length, DEFAULT_ITERATION);
    if (ret != 0) {
        printf("error: deriv_passwd\n");
        return 1;
    }


    ////// then we call our protect_buffer() function
    char* message = "hello world";
    int message_len = strlen(message);
    unsigned char** output = malloc(sizeof(unsigned char*));
    int* output_len = malloc(sizeof(int));
    ret = protect_buffer(output, output_len, (unsigned char*)message, message_len, master_key, KEY_SZ, salt, salt_length);

    // TODO: quelques free()
    return 0;
}
