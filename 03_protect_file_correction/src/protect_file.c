#include "deriv.h"
#include "protect_file.h"

#include "mbedtls/havege.h"
#include "mbedtls/sha256.h"
#include "mbedtls/md.h"
#include "mbedtls/aes.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define BLOCK_SZ    16
#define IV_SZ       BLOCK_SZ
#define HMAC_SZ     32


const unsigned char padding[BLOCK_SZ] = {
	0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};


static void zeroize(void *v, size_t n)
{
	volatile unsigned char *p = v;
	while (n--)
	{
		*p++ = 0;
	}
}

int protect_buffer(unsigned char **output, int *output_len,
				   unsigned char *input, int input_len,
				   unsigned char *master_key, int key_len,
				   unsigned char *salt, int salt_len)
{
	int ret;
	char id_sub_key;
	unsigned char k_c[32];
	unsigned char k_i[32];
	unsigned char tmp_1[33];
	int pad_len;
	unsigned char *input_padd;
	unsigned char *cipher;
	mbedtls_aes_context aes_ctx;
	mbedtls_md_context_t  hmac_sha256_ctx;
	mbedtls_havege_state havege_ctx;
	unsigned char iv_[IV_SZ];
	unsigned char *p;


	/* *** Initialisation *** */
	ret = 1;
	input_padd = NULL;
	cipher = NULL;
	mbedtls_aes_init(&aes_ctx);
	mbedtls_havege_init(&havege_ctx);
	

	/* *** Check des arguments *** */
	if((output == NULL) || (output_len == NULL) || (input == NULL) 
	|| (input_len <= 0) || (master_key == NULL) || (key_len <= 0)
	|| (salt == NULL) || (salt_len <= 0))
	{
		goto cleanup;
	}


	/* *** Deriv MasterKey to CipherKey / IntegrityKey *** */
	id_sub_key = 0;
	memcpy(tmp_1, master_key, key_len);
	memcpy(tmp_1+32, &id_sub_key, sizeof(char));
	mbedtls_sha256(tmp_1, 33, k_c, 0);
	id_sub_key ++;
	memcpy(tmp_1, master_key, key_len);
	memcpy(tmp_1+32, &id_sub_key, sizeof(char));
	mbedtls_sha256(tmp_1, 33, k_i, 0);


	/* *** Padding ?! *** */
	pad_len = 16 - (input_len % 16);
	input_padd = (unsigned char *)malloc((input_len + pad_len)*sizeof(char));
	if(input_padd == NULL)
	{
		goto cleanup;
	}
	cipher = (unsigned char *)malloc((salt_len + IV_SZ + input_len + pad_len + HMAC_SZ)*sizeof(char));
	if(cipher == NULL)
	{
		goto cleanup;
	}
	memcpy(input_padd, input, input_len);
	memcpy(input_padd+input_len, padding, pad_len);


	/* *** Generation de IV *** */
	ret = mbedtls_havege_random(&havege_ctx, iv_, 16);
	if(ret != 0)
	{
		goto cleanup;
	}


	/* *** Construction de cipher *** */
	memcpy(cipher, salt, salt_len);
	memcpy(cipher + salt_len, iv_, IV_SZ);
	

	/* *** Chiffrement *** */
	ret = mbedtls_aes_setkey_enc(&aes_ctx, k_c, 256);
	if(ret != 0)
	{
		goto cleanup;
	}

	ret = mbedtls_aes_crypt_cbc(&aes_ctx, 
								MBEDTLS_AES_ENCRYPT, 
			input_len+pad_len, iv_, input_padd, 
			cipher+salt_len+16);
	if(ret != 0)
	{
		goto cleanup;
	}
	
	
	/* *** Ajout du controle d'integrite *** */
	p = cipher + salt_len + IV_SZ + input_len + pad_len;
	mbedtls_md_hmac(mbedtls_md_info_from_type(MBEDTLS_MD_SHA256), k_i, 32,
				(unsigned char *) cipher, (size_t) salt_len + IV_SZ + input_len + pad_len,
				(unsigned char *)p);


	/* *** On recopie les resultats dans output et output_len *** */
	*output = cipher;
	*output_len = salt_len + IV_SZ + input_len + pad_len + HMAC_SZ;

	ret = 0;
cleanup:
	if(input_padd != NULL)
		free(input_padd);
	mbedtls_aes_free(&aes_ctx);
	mbedtls_havege_free(&havege_ctx);
	zeroize(k_c, 32);
	zeroize(k_i, 32);
	zeroize(tmp_1, 33);

	return ret;
}
