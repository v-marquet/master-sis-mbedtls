TPs
===
* 01: crée une clé random (un salt), d'une taille donnée (`havege.h`)
* 02: SHA256 d'un MDP avec salt (`mbedtls/sha256.h`)
* 03: chiffrement AES + HMAC (`mbedtls/aes.h`)
* projet: ajout de RSA



Projets mbedTLS
===============

Exercice 1 :

*But : 
	Ecrire un logiciel de chiffrement multi-destinataires qui 
	protege un fichier en confidentialite et en integrite.

	Un fois le fichier protege, il est envoye a N destinataires. Si le
	destinataire est legitime, il peut le deprotege, sinon il ne peut rien en
	faire.

	Chaque intervenant possede un bi-cle RSA-2048 pour le
	chiffrement/dechiffrement et un bi-cle RSA-2048 pour la signature.

	Proteger le fichier en confidentialite:
		- Kc : random()
		- IV : random()
		- C = AES-CBC-256(input, Kc, IV)
        - padding = 0x80
		- RSA PKCS#1 OAEP
	Proteger le fichier en integrite:
		- Signer la totalite du message a envoyer
		- RSA PKCS#1 PSS


	Le fichier contenant le chiffré + les métadata doit être au format:
	0x00 || SHA256(kpub-1) || RSA_kpub-1(Kc) || ... || 0x00 || SHA256(kpub-N) || RSA_kpub-N(Kc) || 0x01 || IV || C || Sign
    
    

*Usage pour un participant legitime:
	$ openssl genrsa 2048 > my_ciph_priv.pem [my_ciph_pub.pem]
	$ openssl genrsa 2048 > my_sign_priv.pem [my_sign_pub.pem]

	// Proteger input_file
	$ ./multi_protect -e <input_file> <output_file> <my_sign_priv.pem> <my_ciph_pub.pem> [user1_ciph_pub.pem ... [userN_ciph_pub.pem]]
	// retourne 0 si OK, 1 sinon

	// Deproteger input_file
	$ ./multi_protect -d <input_file> <output_file> <my_priv_ciph.pem> <my_pub_ciph.pem> <sender_sign_pub.pem>
	// retourne 0 si OK, 1 sinon
	


Exercice 2 :
- finir protect_buffer (symetric)
- finir unprotect_buffer (symetric)


-----------------------------------

De facon generale :
    - 1 repertoire par projet (utiliser la meme arborescence que celle
	  presentee en cours
    - la compilation ne doit generer aucune erreur ni warning, un projet qui
	  ne compile pas (erreur ou warning) ne sera pas corrige
    - les outils 'valgrind' et 'scan-build' (LLVM/Clang) doivent etre utilises
    - l'archive envoyee sera 'propre', i.e. elle ne contiendra pas
	  d'executable ou de fichier objets issus de compilation
    - assurez vous que vos projets fonctionnent chez moi (linux x86_64, gcc-4.9,
	  mbedtls a jour)

date de reception (max) : 04/03/2016 @ 23:59
subject : [MSSIS_1516_mbedtls]
mail : olivier.tuchon@gmail.com
