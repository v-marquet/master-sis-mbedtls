#ifndef _DERIV_H
#define _DERIV_H

/**
 * @param [out]             key (32 bytes)
 * @param [in] passwd       user password
 * @param [in] salt         salt
 * @param [in] salt_len     salt length in bytes
 * @param [in] iterations   number of iterations
 *
 * @return  0 if ok, 1 else
 */

int deriv_passwd(unsigned char * key, const char * password, 
                 const unsigned char * salt, const int salt_len, 
                 const unsigned int iterations);

#endif


