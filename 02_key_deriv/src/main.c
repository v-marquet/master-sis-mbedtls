#include "mbedtls/sha256.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int deriv_passwd(unsigned char * key, const char * password, 
                 const unsigned char * salt, const int salt_len, 
                 const unsigned int iterations)
{
    // TODO: check args

    unsigned char digest[32];
    int i = 0;

    mbedtls_sha256_context context;
    mbedtls_sha256_init(&context);

    mbedtls_sha256_starts(&context, 0);

    // compute H0
    mbedtls_sha256_update(&context, (const unsigned char*)password, strlen(password));
    mbedtls_sha256_update(&context, salt, salt_len);
    mbedtls_sha256_update(&context, (unsigned char *)&i, 4);

    mbedtls_sha256_finish(&context, digest);

    for(i=0; i<iterations; i++) {
        mbedtls_sha256_starts(&context, 0);

        // compute Hi
        mbedtls_sha256_update(&context, (const unsigned char*)digest, 32);
        mbedtls_sha256_update(&context, (const unsigned char*)password, strlen(password));
        mbedtls_sha256_update(&context, salt, salt_len);
        mbedtls_sha256_update(&context, (unsigned char *)&i, 4);

        mbedtls_sha256_finish(&context, digest);
    }

    memcpy(key, digest, 32);
    mbedtls_sha256_free(&context);
    return 0;
}


int main(int argc, char const *argv[])
{
    const char* password = "test";
    const char* salt = "AAAAAAAA"; // à faire avec havege_random
    int salt_len  = strlen(salt);
    unsigned char key[32];

    deriv_passwd(key, password, (const unsigned char*)salt, salt_len, 32);

	return 0;
}
