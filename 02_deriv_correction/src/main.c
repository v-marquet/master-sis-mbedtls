#include "deriv.h"
#include "mbedtls/havege.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define SALT_SZ     		8
#define KEY_SZ      		32
#define DEFAULT_ITERATION 	6000


int print_hex(
		unsigned char *buffer, 
		int buffer_len,
		char *id)
{
	int i;

	printf(">>> %s\n", id);
	for(i = 0; i < buffer_len; i++)
		printf("%02X", buffer[i]);
	printf("\n");
	
	return 0;
}


static void zeroize(void *v, size_t n)
{
	volatile unsigned char *p = v;
	while (n--)
	{
		*p++ = 0;
	}
}


int main(int argc, char *argv[])
{
	int ret;
	
	unsigned char key[KEY_SZ];
	unsigned char salt[SALT_SZ];
	char *password;
	unsigned int iterations;
	mbedtls_havege_state ctx;


	/* *** initialization *** */
	password = argv[1];
	iterations = DEFAULT_ITERATION;
	ret = 1;
	
	
	/* *** gen salt *** */
	mbedtls_havege_init(&ctx);
	ret = mbedtls_havege_random(&ctx, salt, SALT_SZ);
	if(ret != 0)
		goto cleanup;

	/* *** deriv password *** */
	ret = deriv_passwd(key, password, salt, SALT_SZ, iterations);
	if(ret != 0)
		goto cleanup;
	
	/* *** print salt & key *** */
	print_hex(salt, SALT_SZ, "salt");
	print_hex(key, KEY_SZ, "key = ");
	
	
	ret = 0;
cleanup:
	/* *** cleanup and return *** */
	mbedtls_havege_free(&ctx);
	zeroize(key, KEY_SZ);
	zeroize(salt, SALT_SZ);

	return ret;
}

